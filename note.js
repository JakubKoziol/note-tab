document.addEventListener("DOMContentLoaded", function(){
	if(localStorage.getItem("note")) {
		document.getElementById("note").innerText = localStorage.note;
	}

	// DOMSubtreeModified instead of DOMCharacterDataModified as a workaround for single-character deletion not firing
	document.getElementById("note").addEventListener("DOMSubtreeModified", function () {
		localStorage.note = document.getElementById("note").innerText;
	}, false);
});